

                              METROPOLY 4 - 3DS MAX RIGGED CHARACTERS               
                                            
                                           SPECIAL NOTES


******************************************************************************************************
******************************************************************************************************
* STANDARD MATERIAL SETUP:                                                                           *
*                                                                                                    *
* a) Files have been created with 3DS MAX 2010 SP2.                                                  *  
* b) To show METROPOLY 4 viewport shader your 3DS MAX should be configured using the Direct3D Driver.*
*    MODELS WILL APPEAR "GREY" IF YOUR SYSTEM USES THE OPENGL DRIVER.                                *
*                                                                                                    *
*    To change this settings go to:                                                                  *
*    CUSTOMIZE > PREFERENCES > VIEWPORT > CHOOSE DRIVER > REVERT FROM OPENGL > DIRECT3D (Recomended).*
*                                                                                                    *
*                                                                                                    *
* DIRECTX AND RENDERABLE MATERIAL:                                                                   *
*                                                                                                    *
* a) The first part of the material is the DirectX Shader used for the viewport realtime displaying. *
*                                                                                                    *
* b) The Renderable material is found at the bottom slot called "Software Render Style".             *
*    They are called "StandardFX_renderstyle (Standard)".                                            *
*                                                                                                    *
*                                                                                                    *
******************************************************************************************************
******************************************************************************************************
* MENTAL RAY MATERIAL SETUP:                                                                         *
*                                                                                                    *
* a) Files have been created with 3DS MAX 2010 SP2.                                                  *  
* b) Files were set up for Gamma 2.2                                                                 *
* c) If you import the model into a scene without gamma 2.2 you should modifiy the normal maps RGB   *
*    Level in the bump slot of each material as follows:                                             *
*                                                                                                    *
*    With Gamma 2.2 --> RGB Level = 2.2                                                              *
*    With Gamma 1.0 --> RGB Level = 1                                                                *
*                                                                                                    *
******************************************************************************************************
******************************************************************************************************
* VRAY MATERIAL SETUP:                                                                               *
*                                                                                                    *
* a) Files have been created with 3DS MAX 2010 SP2 + V-Ray 3.0                                       *                 
* b) Files were set up for Gamma 2.2                                                                 *
* c) If you import the model into a scene without gamma 2.2 you should modifiy the normal maps RGB   *
*    Level in the bump slot of each material as follows:                                             *
*                                                                                                    *
*    With Gamma 2.2 --> RGB Level = 2.2                                                              *
*    With Gamma 1.0 --> RGB Level = 1                                                                *
*                                                                                                    *
******************************************************************************************************
******************************************************************************************************
* MATERIAL IDs:                                                                                      *
*                                                                                                    *
*                                                                                                    *
* a) Each METROPOLY 3 Character is modeled using an unique mesh.                                     *
*    The material is a Multi/Sub-Object with the folowing Polygon Material IDs:                      *
*                                                                                                    *
*    1. Hair                                                                                         *
*    2. Head                                                                                         *
*    3. Eyes&Mouth                                                                                   *
*    4. Skin                                                                                         *
*    5. Cloths&Stuff                                                                                 *
*                                                                                                    *
******************************************************************************************************
                                                                           
                                 Copyright (c) 2015 - aXYZ design
                                        www.axyz-design.com                                                                   