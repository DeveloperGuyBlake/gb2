using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizButton : MonoBehaviour
{
    public bool isCorrectAnswer = false;
    public GameObject currentQuestionPanel;
    public GameObject nextQuestionPanel;

    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void ClickButton()
    {
        if (isCorrectAnswer)
        {
            anim.SetTrigger("Green");
            StartCoroutine("CorrectAnswer");
        }
        else anim.SetTrigger("Red");
    }

    IEnumerator CorrectAnswer()
    {
        yield return new WaitForSeconds(2);
        nextQuestionPanel.SetActive(true);
        currentQuestionPanel.SetActive(false);
    }
}
