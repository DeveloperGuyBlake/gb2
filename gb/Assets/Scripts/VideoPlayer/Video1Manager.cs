using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Video1Manager : MonoBehaviour
{
    public GameObject quizPanel;
    public float waitTime = 27;
    float startTime;
    bool videoEnded = false;

    void Start()
    {
        startTime = Time.time;
        GameManager.gameManager.ToggleAimVis(false);
    }
    void Update()
    {
        if (!videoEnded && Time.time >= startTime+waitTime)
        {
            videoEnded = true;
            quizPanel.SetActive(true);
            GameManager.gameManager.ToggleAimVis(true);
        }
    }
}
