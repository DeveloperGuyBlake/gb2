using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PressureManager : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject guessPanel;
    public GameObject endPanel;
    int correctGuesses = 0;//how many correct guesses the user has made.

    ////GENERATE PATIENT VALUES////
    public string[] firstNames;
    public string[] lastNames;
    string generatedName;
    int generatedAge;
    int patientState; //0=low, 1=normal, 2=high pressure
    int generatedTopVal;
    int generatedBottomVal;

    ////CLIPBOARD/READOUT OBJECTS////
    public Text pressureReadoutText;
    public Text patientNameText;
    public Text patientAgeText;


    void Awake()
    {
        GeneratePatient();
    }

    public void GeneratePatient()
    {
        int randF = Random.Range(0,firstNames.Length);
        int randL = Random.Range(0,lastNames.Length);
        generatedName = lastNames[randL] + ", " + firstNames[randF];
        patientNameText.text = "Patient Name: " + generatedName;
        generatedAge = Random.Range(20,65);//Patient age will never be 65 because highest int in Random.Range is exclusive.
        patientAgeText.text = "Patient Age: " + generatedAge.ToString();
        pressureReadoutText.text = "-\n-";
    }
    public void GeneratePressure()
    {
        patientState = Random.Range(0,3);//33% chance to generate a high, low, or normal number. (high int in random.range is exclusive)
        if (generatedAge >= 20 && generatedAge <= 24)//TODO: Clean up this section using dynamic number ranges instead of manually determining every age value ranges.
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(98,108);
                generatedBottomVal = Random.Range(65,75);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(110,130);
                generatedBottomVal = Random.Range(77,82);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(133,143);
                generatedBottomVal = Random.Range(84,87);
            }
        }
        if (generatedAge >= 25 && generatedAge <= 29)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(98,108);
                generatedBottomVal = Random.Range(71,75);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(114,128);
                generatedBottomVal = Random.Range(78,83);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(134,144);
                generatedBottomVal = Random.Range(85,90);
            }
        }
        if (generatedAge >= 30 && generatedAge <= 34)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(100,109);
                generatedBottomVal = Random.Range(71,76);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(120,126);
                generatedBottomVal = Random.Range(80,84);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(135,145);
                generatedBottomVal = Random.Range(86,90);
            }
        }
        if (generatedAge >= 35 && generatedAge <= 39)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(100,110);
                generatedBottomVal = Random.Range(73,77);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(120,125);
                generatedBottomVal = Random.Range(80,85);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(136,146);
                generatedBottomVal = Random.Range(87,93);
            }
        }
        if (generatedAge >= 40 && generatedAge <= 44)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(101,111);
                generatedBottomVal = Random.Range(74,79);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(123,130);
                generatedBottomVal = Random.Range(81,86);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(0,1);
                generatedBottomVal = Random.Range(0,1);
            }
        }
        if (generatedAge >= 45 && generatedAge <= 49)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(105,115);
                generatedBottomVal = Random.Range(74,80);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(125,130);
                generatedBottomVal = Random.Range(82,87);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(140,150);
                generatedBottomVal = Random.Range(89,95);
            }
        }
        if (generatedAge >= 50 && generatedAge <= 54)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(105,115);
                generatedBottomVal = Random.Range(75,80);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(127,133);
                generatedBottomVal = Random.Range(83,88);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(143,153);
                generatedBottomVal = Random.Range(90,95);
            }
        }
        if (generatedAge >= 55 && generatedAge <= 59)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(107,117);
                generatedBottomVal = Random.Range(76,81);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(128,136);
                generatedBottomVal = Random.Range(84,88);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(145,155);
                generatedBottomVal = Random.Range(90,96);
            }
        }
        if (generatedAge >= 60 && generatedAge <= 64)
        {
            if (patientState == 0)
            {
                generatedTopVal = Random.Range(110,120);
                generatedBottomVal = Random.Range(75,82);
            }
            else if (patientState == 1)
            {
                generatedTopVal = Random.Range(132,139);
                generatedBottomVal = Random.Range(85,90);
            }
            else if (patientState == 2)
            {
                generatedTopVal = Random.Range(148,158);
                generatedBottomVal = Random.Range(92,100);
            }
        }
        StartCoroutine(PressureRoutine());
    }
    IEnumerator PressureRoutine()
    {
        for (int x = 0; x < 10; x++)//animate random numbers on the display for a bit
        {
            int y = Random.Range(100,180);
            int z = Random.Range(60,120);
            pressureReadoutText.text = y + "\n" + z;
            yield return new WaitForSeconds(0.2f);
        }
        pressureReadoutText.text = "-\n-";
        yield return new WaitForSeconds(0.5f);
        pressureReadoutText.text = generatedTopVal + "\n" + generatedBottomVal;
    }
}
