using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;
using UnityEngine.UI;

namespace BFVR.InputModule
{
    /// <summary>
    /// Standard BFVR hand tracking controller. Keeps track of user's hand position using the OpenXR framework.
    /// </summary>
    public class BFVRHandTrackingController : MonoBehaviour
    {
        [Tooltip("Set true for right hand controls. Defaults to left hand.")]
        public bool rightHand = false;
        public Transform laserPoint;

        Transform HandRoot;
        Vector3 HandPos;
        Quaternion HandRotation;
        
        LayerMask mask;
        RaycastHit hit;


        void OnEnable()
        {
            BFVRInputManager.uiOnClickPerformedEvent += OnClickPerformed;
        }
        void OnDisable()
        {
            BFVRInputManager.uiOnClickPerformedEvent -= OnClickPerformed;
        }
        void OnClickPerformed()
        {
            Debug.Log("clickPerformed");
            if (rightHand && Physics.Raycast(laserPoint.position, laserPoint.TransformDirection(Vector3.up), out hit, Mathf.Infinity, mask))//raycast to find UI buttons
            {
                Debug.Log("InvokedButton");
                hit.collider.gameObject.GetComponent<Button>().onClick.Invoke();//invoke UI button click
            }
        }

        private void Start()
        {
            InitializeXRSubsystem();
            if (rightHand)
            {
                mask = LayerMask.GetMask("RaycastTarget_Button");
            }
        }

        private void Update()
        {
            UpdateHandTracking();
        }

        void InitializeXRSubsystem()
        {
            List<XRInputSubsystem> subsystems = new List<XRInputSubsystem>();
            SubsystemManager.GetInstances<XRInputSubsystem>(subsystems);
            for(int i = 0; i <subsystems.Count;i++)
            {
                subsystems[i].TrySetTrackingOriginMode(TrackingOriginModeFlags.Floor);
            }
        }

        void UpdateHandTracking()
        {
            HandPos = Vector3.zero;
            HandRotation = Quaternion.identity;

            List<XRNodeState> nodeStates = new List<XRNodeState>();
            InputTracking.GetNodeStates(nodeStates);

            foreach(XRNodeState nodeState in nodeStates)
            {
                if(rightHand && nodeState.nodeType == XRNode.RightHand)
                {
                    nodeState.TryGetPosition(out HandPos);
                    nodeState.TryGetRotation(out HandRotation);
                    break;
                }
                else if(!rightHand && nodeState.nodeType == XRNode.LeftHand)
                {
                    nodeState.TryGetPosition(out HandPos);
                    nodeState.TryGetRotation(out HandRotation);
                    break;
                }
            }

            transform.localPosition = HandPos;
            transform.localRotation = HandRotation;
            
        }
    }
}