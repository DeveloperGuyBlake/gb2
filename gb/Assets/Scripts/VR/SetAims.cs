using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAims : MonoBehaviour
{
    public GameObject lAim;
    public GameObject rAim;

    void Start()
    {
        //GameManager.gameManager.SetAims(false, lAim);
        GameManager.gameManager.SetAims(true, rAim);
    }
}
