using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    GameObject rhAim;
    //GameObject lhAim;

    void Awake()
    {
        if (gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else Destroy(this);
    }

    public void ToggleAimVis(bool b)
    {
        rhAim.SetActive(b);
        //lhAim.SetActive(b);
    }

    public void SetAims(bool rightHand, GameObject g)
    {
        if (rightHand) rhAim = g;
        //else lhAim = g;
    }
}
