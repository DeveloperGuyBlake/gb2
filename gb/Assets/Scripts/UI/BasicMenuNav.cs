using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMenuNav : MonoBehaviour
{
    public GameObject[] showObjects;
    public GameObject[] hideObjects;

    public void RunNav()
    {
        foreach (GameObject a in showObjects) a.SetActive(true);
        foreach (GameObject b in hideObjects) b.SetActive(false);
    }
}
